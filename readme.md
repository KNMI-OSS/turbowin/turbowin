**download binaries/executables (free and open-source):**

  _TurboWin+ GA (General-Availability)_
  - [TurboWin+ V4.5 Windows 64 bit (44 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_5_hl.exe)*
  - [TurboWin+ V4.5 Windows 32 bit (28 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_4_5_hl.exe)
  - [TurboWin+ V4.5 Linux 64 bit [rpm] (50 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/TurboWin+-4.5.0-1.x86_64.rpm)
  - [TurboWin+ V4.5 Linux 64 bit [deb] (50 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/TurboWin+_4.5.0-1.deb)
  - [TurboWin+ V3.3.0 Linux 32 bit (117 Mb; Java JRE 7 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/TurboWin+.zip)
      <br>  * Windows 11 by default.

  _TurboWin+ RC (Release Candidate; not for operational use; beta)_
  - [TurboWin+ V4.6 Windows 64 bit (55 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_6_beta_hl.exe)
 
  _TurboWin+ earlier versions_
   - [TurboWin+ V4.4 Windows 64 bit (44 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_4_hl.exe)
  - [TurboWin+ V4.4 Windows 32 bit (28 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_4_4_hl.exe)
  - [TurboWin+ V4.3 Windows 64 bit (44 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_3_hl.exe)
  - [TurboWin+ V4.3 Windows 32 bit (27 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_4_3_hl.exe)
  - [TurboWin+ V4.2 Windows 64 bit (145 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_hl.exe)**
  - [TurboWin+ V4.2 Windows 32 bit (132 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+.exe)**
  - [TurboWin+ V4.1 Windows 64 bit (138 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_1_hl.exe)
  - [TurboWin+ V4.1 Windows 32 bit (125 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_4_1.exe)
  - [TurboWin+ V4.0 Windows 64 bit (138 Mb; no Java required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_jpms_4_0_hl.exe)
  - [TurboWin+ V4.0 Windows 32 bit (125 Mb; Java JRE 8 or higher required)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/setup_turbowin+_4_0.exe)
      <br>  ** During installation: if the destination location (folder) already exists, delete this folder first or select a different folder.

  _TurboWin (End-of-support has been followed by TurboWin+)_
  - [TurboWin V5.6 (Windows XP/Vista/7/8.1/10) (513 Mb)](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/TW56.zip)

  _EUCAWS SMD output simulation program (Meteo France)_
  - [SMDoutput_V4](https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/raw/master/binaries/SMDoutput_V4.exe)


**Issues list labels explanation:** https://gitlab.com/KNMI-OSS/turbowin/turbowin/-/blob/master/TurboWin-Gitlab-Labels_examples.pdf

**Contact:** turbowin'at'knmi'dot'nl
