/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package turbowin;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author marti
 */
public class pop_up_screen extends javax.swing.JFrame {
   
   

   /**
    * Creates new form pop_up_screen
    */
   public pop_up_screen(boolean in_screen_pop_up_period) throws IOException {
      
      initComponents();
      initComponents2();
      setLocation(main.x_pos_pop_up_frame, main.y_pos_pop_up_frame);
      local_in_screen_pop_up_period = in_screen_pop_up_period;
   }

   /**
    * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
    * Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents()
   {

      jButton1 = new javax.swing.JButton();
      jButton2 = new javax.swing.JButton();
      jSeparator1 = new javax.swing.JSeparator();
      jLabel1 = new javax.swing.JLabel();
      jLabel2 = new javax.swing.JLabel();

      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
      setResizable(false);
      addWindowListener(new java.awt.event.WindowAdapter()
      {
         public void windowClosed(java.awt.event.WindowEvent evt)
         {
            pop_up_screen_windowClosed(evt);
         }
         public void windowClosing(java.awt.event.WindowEvent evt)
         {
            pop_up_screen_windowClosing(evt);
         }
      });

      jButton1.setText("OK");
      jButton1.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            OK_button_actionPerformed(evt);
         }
      });

      jButton2.setText("Cancel");
      jButton2.addActionListener(new java.awt.event.ActionListener()
      {
         public void actionPerformed(java.awt.event.ActionEvent evt)
         {
            Cancel_button_actionPerformed(evt);
         }
      });

      jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
      jLabel2.setText("add visual observation prior to the next automated upload");

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                  .addContainerGap()
                  .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(layout.createSequentialGroup()
                  .addGap(112, 112, 112)
                  .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(11, Short.MAX_VALUE))
         .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(4, 4, 4)
            .addComponent(jLabel2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(15, 15, 15)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(15, 15, 15))
      );

      pack();
   }// </editor-fold>//GEN-END:initComponents

   
   
   
   /***********************************************************************************************/
   /*                                                                                             */
   /*                                                                                             */
   /*                                                                                             */
   /***********************************************************************************************/
   private void Cancel_button_actionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_Cancel_button_actionPerformed
   {//GEN-HEADEREND:event_Cancel_button_actionPerformed
      // TODO add your handling code here:
      
      pop_up_screen_windowClosing(null);
   }//GEN-LAST:event_Cancel_button_actionPerformed

   
   
   /***********************************************************************************************/
   /*                                                                                             */
   /*                                                                                             */
   /*                                                                                             */
   /***********************************************************************************************/
   private void pop_up_screen_windowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_pop_up_screen_windowClosing
   {//GEN-HEADEREND:event_pop_up_screen_windowClosing
      // TODO add your handling code here:
      
      setVisible(false);
      dispose(); // NB this will NOT set implicit main.pop_up_form = null
   }//GEN-LAST:event_pop_up_screen_windowClosing

   
   
   /***********************************************************************************************/
   /*                                                                                             */
   /*                                                                                             */
   /*                                                                                             */
   /***********************************************************************************************/
   private void OK_button_actionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OK_button_actionPerformed
      // TODO add your handling code here:
      
      this.setAlwaysOnTop(false);                                    // reset visual obs pop-up screen option to 'NOT aways on top', otherwise main screen cannot be opened this way
      //System.out.println("--- APR pop-up screen (re)set to 'NOT Always On Top'");
      
      if (main.mainClass != null)
      {   
         if ((turbowin.main.ICONIFIED & main.mainClass.getExtendedState()) == turbowin.main.ICONIFIED)
         {
            if (turbowin.main.trayIcon != null)
            {
               //main.mainClass.tray.remove(turbowin.main.trayIcon) ; 
               turbowin.main.tray.remove(turbowin.main.trayIcon) ;
            }
         }
         
         main.mainClass.setExtendedState(NORMAL);
         main.mainClass.setVisible(true); 
         
      } // if (main.mainClass != null)
      
      pop_up_screen_windowClosing(null);                             // delete thid pop-up screen
   }//GEN-LAST:event_OK_button_actionPerformed
   
   
   
   /***********************************************************************************************/
   /*                                                                                             */
   /*                                                                                             */
   /*                                                                                             */
   /***********************************************************************************************/   
   private void pop_up_screen_windowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_pop_up_screen_windowClosed
      // TODO add your handling code here:
      
      if (local_in_screen_pop_up_period)
      {
         main.screen_was_manually_closed_in_pop_up_period = true; 
      }
      else
      {
         main.screen_was_manually_closed_in_pop_up_period = false;
      }

      //System.out.println("screen_was_manually_closed_in_pop_up_period = " + main.screen_was_manually_closed_in_pop_up_period);
      System.out.println("pop-up screen was closed");
      
      
      main.pop_up_form = null;
   }//GEN-LAST:event_pop_up_screen_windowClosed

   
   
/***********************************************************************************************/
/*                                                                                             */
/*                                                                                             */
/*                                                                                             */
/***********************************************************************************************/
private void initComponents2() 
{
   // nb could also be set in design mode but then this will be a fixed char string even if the APPLICATION_NAME was changed
   setTitle("reminder " + main.APPLICATION_NAME);
   
   try
   {   
      URL iconURL = getClass().getResource(main.ICONS_DIRECTORY + "tray.png");
      ImageIcon icon = new ImageIcon(iconURL);
      setIconImage(icon.getImage());
   }
   catch (Exception e) 
   { 
      System.out.println("--- pop-up screen frame icon not found");
   }
   
   // pop-up screen logo 
   loadImage_pop_up_logo(main.ICONS_DIRECTORY + "logo-sot.png");
   
   // initialisation
   main.screen_was_manually_closed_in_pop_up_period = false;
   
   // Opacity
   // NB this.setOpacity(0.8f);                       // NB only possible if JFrame not decorated! (upper 'beam' in screen, so then no screen moving or closing with 'x' possible)
   
   try
   {
      //setAlwaysOnTop(true);
      //System.out.println("--- pop-up screen visual obs reminder opened and set to 'Always On Top'");
            
      // move mouse pointer to centre screen to deactivate screensaver if screensaver is active
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      int centerX = screenSize.width / 2;
      int centerY = screenSize.height / 2;
      Robot robot = null;
      try 
      {
         robot = new Robot();
         robot.mouseMove(centerX, centerY);
      } 
      catch (AWTException e) 
      {
         main.log_turbowin_system_message("[GENERAL] pop-up screen visual obs reminder: error mouse move for deactivating screensaver (" + e.getCause().toString() + ")");
      }
   } // try
   catch (SecurityException ex)
   {
      //main.log_turbowin_system_message("[GENERAL] pop-up screen visual obs reminder: there is no permission to set the value of the always-on-top property (" + ex.getCause().toString() + ")");
      main.log_turbowin_system_message("[GENERAL] pop-up screen visual obs reminder error: (" + ex.getCause().toString() + ")");
   }
   
   // NB requestFocus(): This is not guaranteed to work, because there are many reasons why an operating system would not allow a frame to have focus
   requestFocus();

   System.out.println("pop-up screen was opened");
}
   

   
/***********************************************************************************************/
/*                                                                                             */
/*                                                                                             */
/*                                                                                             */
/***********************************************************************************************/
private void loadImage_pop_up_logo(final String imagePath) 
{
   new SwingWorker<ImageIcon, Object>() 
   {
      @Override
      public ImageIcon doInBackground() 
      {
         return createImageIcon(imagePath);
      }

      @Override
      public void done()
      {
         try
         {     
            ImageIcon logo_icon = get();
            jLabel1.setIcon(logo_icon); 
         } // try
         catch (InterruptedException ignore) { }
         catch (java.util.concurrent.ExecutionException e) 
         {
            String why;
            Throwable cause = e.getCause();
            if (cause != null) 
            {
               why = cause.getMessage();
            } 
            else 
            {
               why = e.getMessage();
            }
            //System.err.println("Error retrieving file: " + why);
            JOptionPane.showMessageDialog(null, "Error retrieving file: " + why, main.APPLICATION_NAME, JOptionPane.ERROR_MESSAGE);
         } // catch         
      } //  public void done()
   }.execute();
} // private void loadImage(final String imagePath, final int index)

   
   
/***********************************************************************************************/
/*                                                                                             */
/*                                                                                             */
/*                                                                                             */
/***********************************************************************************************/
public ImageIcon createImageIcon(String path_and_file)
{
   URL url = null;

   try
   {
      url = getClass().getResource(path_and_file);
   }
   catch (Exception e) { /* ... */}

   ImageIcon icon_glyph = new javax.swing.ImageIcon(url);

   return icon_glyph;
}   
   

   
   /**
    * @param args the command line arguments
    */
   public static void main(String args[]) {
      java.awt.EventQueue.invokeLater(new Runnable() {
         @Override
         public void run() {
            try {
               new pop_up_screen(local_in_screen_pop_up_period).setVisible(true);
            } 
            catch (IOException ex) { /*  */ }
         }
     });
   }

   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton jButton1;
   private javax.swing.JButton jButton2;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JLabel jLabel2;
   private javax.swing.JSeparator jSeparator1;
   // End of variables declaration//GEN-END:variables

   private static boolean local_in_screen_pop_up_period;
        
}
